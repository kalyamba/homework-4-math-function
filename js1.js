
let num1 = Number (prompt('Введіть перше число'));

let num2 = Number (prompt('Введіть друге число'));


let operation = prompt('Введите операцию `+`, `-`, `*`, `/`');

function calculator(num1, num2, operation) {
    switch (operation) {
        case '+':
            return num1 + num2;
        case '-':
            return num1 - num2;
        case '*':
            return num1 * num2;
        case '/':
            return num1 / num2;
    }
}
console.log(`Результат: ${calculator(num1, num2, operation)}`);